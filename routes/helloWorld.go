package routes

import (
	"FiberBasic/handlers"
	"FiberBasic/middleware"
	swagger "github.com/arsmn/fiber-swagger/v2"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/monitor"
)

func New() *fiber.App {
	app := fiber.New()
	app.Use(cors.New())
	app.Get("/dashboard", monitor.New())

	api := app.Group("/api")
	api.Use(logger.New())
	api.Get("/swagger/*", swagger.HandlerDefault)
	api.Get("/", middleware.AuthMiddleware, handlers.HelloWorldHandler)

	return app
}
