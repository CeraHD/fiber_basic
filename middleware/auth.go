package middleware

import (
	"FiberBasic/responses"
	"github.com/gofiber/fiber/v2"
)

func AuthMiddleware(ctx *fiber.Ctx) error {
	authHeader := ctx.Get("Authorization", "")
	if authHeader == "" {
		return ctx.Status(fiber.StatusForbidden).JSON(&responses.Response{Message: "Unauthorized"})
	}
	return ctx.Next()
}
