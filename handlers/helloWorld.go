package handlers

import (
	"FiberBasic/responses"
	"github.com/gofiber/fiber/v2"
)

// Helloworld is a function that returns hello world message as json
// @Summary Hello world message
// @Description Hello world message
// @Tags hello world
// @Accept json
// @Produce json
// @Success 200 {object} responses.Response{}
// @Failure 500 {object} responses.Response{}
// @Failure 403 {object} responses.Response{}
// @Router / [get]
func HelloWorldHandler(ctx *fiber.Ctx) error {
	return ctx.JSON(&responses.Response{Message: "Hello world"})
}
