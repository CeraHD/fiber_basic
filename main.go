package main

import (
	_ "FiberBasic/docs"
	"FiberBasic/routes"
	"os"
)

// @title           Swagger Example API
// @version         1.0
// @description     This is a sample server celler server.
// @termsOfService  http://swagger.io/terms/

// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html

// @host      localhost:8080
// @BasePath  /api

// @securityDefinitions.apiKey  Bearer
// @in header
// @name Authorization
func main() {

	app := routes.New()

	err := app.Listen(":8080")
	if err != nil {
		os.Exit(1)
	}
}
